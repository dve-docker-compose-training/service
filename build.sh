#!/bin/bash

cd training-service/
mvn clean package
cd -

docker build -t vergisond/compose-training:1.0.0 .

docker-compose --file compose-D.yaml up -d
