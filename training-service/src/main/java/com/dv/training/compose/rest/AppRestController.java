package com.dv.training.compose.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppRestController {
    boolean working = true;

    @Value("${greeting}")
    String greeting;

    @RequestMapping("/hello")
    @ResponseBody
    public ResponseEntity hello() {
        if (working) return new ResponseEntity(greeting, HttpStatus.OK);
        return  timeout();
    }

    @RequestMapping("/stop")
    @ResponseBody
    public ResponseEntity stop() {
        working = false;
        return new ResponseEntity("ok",HttpStatus.OK);
    }

    @RequestMapping("/start")
    @ResponseBody
    public ResponseEntity start() {
        working = true;
        return new ResponseEntity("ok",HttpStatus.OK);
    }

    @RequestMapping("/_health")
    @ResponseBody
    public ResponseEntity health() {
        if (working) return new ResponseEntity(HttpStatus.NO_CONTENT);
        return  timeout();
    }

    private ResponseEntity timeout(){
        try {
            Thread.sleep(2*1000*60);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            return new ResponseEntity("you should not see this",HttpStatus.OK);
        }
    }
}
