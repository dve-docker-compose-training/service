FROM java:openjdk-8-jre-alpine

RUN apk add --no-cache curl

COPY training-service/target/compose-1.0-SNAPSHOT.jar /service.jar

COPY run.sh /run.sh
RUN chmod +x /run.sh

ENTRYPOINT /run.sh
