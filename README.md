# Docker compose

## Exemple 1 : réseau inter-conteneurs

> compose-A.yaml

```yaml
---
version: '3'

services:
  web1:
    image: nginx
    ports:
      - "80"
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:
  web2:
    image: nginx
    ports:
      - "80"
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:

networks:
  privatelan:
```

Ici 2 conteneur nginx vont être instanciés et servir leurs contenus sur le port 80 (port aléatoire sur l'hôte).

```sh
# Demonstration des appels depuis l'hôte vers les conteneurs
$ curl localhost:$(docker inspect compose_web1_1 | jq --raw-output '.[0].NetworkSettings.Ports."80/tcp"[0].HostPort')
$ curl localhost:$(docker inspect compose_web2_1 | jq --raw-output '.[0].NetworkSettings.Ports."80/tcp"[0].HostPort')

# Demonstration d'un appel inter-conteneur
## installation de curl dans web1
$ docker exec -it compose_web1_1 apt-get update
$ docker exec -it compose_web1_1 apt-get install -y curl
## recuperation de l'hostname de web2
$ docker exec -it compose_web2_1 hostname
161ed6507629
## appel de web2 depuis web1
$ docker exec -it compose_web1_1 curl 161ed6507629
```

Si on supprime les lignes "ports" comme suit, les conteneurs ne sont plus accessible depuis la machine hôte (il n'y a plus de mapping), mais peuvent toujours communiquer entre eux.

> compose-B.yaml

```yaml
---
version: '3'

services:
  web1:
    image: nginx
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:
  web2:
    image: nginx
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:

networks:
  privatelan:
```

Exercice : essayer
Note : vous pouvez vous faciliter la vie en utilisant le paramètre hostname :

> compose-C.yaml

```yaml
---
version: '3'

services:
  web1:
    image: nginx
    hostname: web1
    ports:
      - "80"
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:
  web2:
    image: nginx
    hostname: web2
    ports:
      - "8080"
    environment:
      - NGINX_PORT=80
    networks:
      privatelan:

networks:
  privatelan:
```

## Exemple 2 : les healthchecks + depends_on

Il est possible de paramétrer docker pour qu'il monitore l'état de santé d'un conteneur.

> compose-D.yaml

```yaml
---
version: '3'

services:
  web1:
    image: vergisond/compose-training:1.0.0
    hostname: service
    ports:
      - "8080"
    environment:
      - GREETING=Hello
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/_health"]
      interval: 10s
      timeout: 1s
      retries: 3
```